#include <systemc.h>
#include <iostream>
#include "datapath.hpp"

using namespace std;
using namespace sc_core;

const sc_time clk_semiperiod(1, SC_NS);
const unsigned tot_istr = 9;

SC_MODULE(TestBench) 
{
 public:
  sc_signal<bool> clk;
  sc_signal<bool> rst_n;
  sc_signal<sc_bv<32> > one;

  datapath dp;

  SC_CTOR(TestBench) : dp("dp")
  {
    SC_THREAD(clk_thread);
    SC_THREAD(rst_thread);
    SC_THREAD(watcher_thread);
      sensitive << clk;

    dp.clk(this->clk);
    dp.rst_n(this->rst_n);
    dp.one(this->one);

  }

  bool check() 
  {
    bool error = 0;
    if(dp.RF_Loc(1).to_uint() != 3 || dp.RF_Loc(2).to_uint() != 8 || dp.RF_Loc(3).to_uint() != 8 || 
       dp.RF_Loc(4).to_uint() != 11 || dp.RF_Loc(5).to_uint() != 0 || dp.RF_Loc(6).to_uint() != 11 ||
       dp.DM_Loc(3).to_uint() != 8)
    {
      cout << "test failed!!\nThe couples of theoretical values or the registers of regster file" 
           << "and data memory - test's values are:" << "\n3-" << dp.RF_Loc(1).to_uint() << "\n8-" 
           << dp.RF_Loc(2).to_uint() << "\n8-" << dp.RF_Loc(3).to_uint() << "\n11-" 
           << dp.RF_Loc(4).to_uint() << "\n0-" << dp.RF_Loc(5).to_uint() << "\n11-" 
           << dp.RF_Loc(6).to_uint() << "\n8-" << dp.DM_Loc(3).to_uint() << endl << endl;
      error = 1;
    }
    else cout << "datapath CORRECT!!" << endl;

    return error;
  }

  void watcher_thread()
  {
    one.write(1);
    while(true)
    {
      wait();
      wait(1, SC_PS);
      if(clk.read() && rst_n.read())
      {
          cout << "At time " << sc_time_stamp() << " in the datapath there are these signals:" 
               << endl;
          cout << "PC out: " << dp.PC_out.read() << endl;
          cout << "Adder PC out: " << dp.PC1.read() << endl;
          cout << "IM out: " << dp.Instruction.read() << endl;
          cout << "Signal mux5A is: " << dp.mux5A.read() << endl;
          cout << "Write address of RF: " << dp.Write_Reg_RF.read() << endl;
          cout << "Regwrite: " << dp.RegWrite.read() << endl;
          cout << "RF out 1: " << dp.RF_out1.read() << endl;
          cout << "RF out 2: " << dp.RF_out2.read() << endl;
          cout << "sign extend out: " << dp.Sign_ext_out.read() << endl;
          cout << "ALU in 2: " << dp.ALU_in.read() << endl;
          cout << "ALU control out: " << dp.ALU_ctrl_out.read() << endl;
          cout << "ALU out: " << dp.ALU_out.read() << endl;
          cout << "DM out: " << dp.DM_out.read() << endl;
          cout << "MEmtoreg: " << dp.MemtoReg.read() << endl;
          cout << "Write data of RF: " << dp.Write_data.read() << endl;
          cout << "Adder branch out: " << dp.adder_out.read() << endl;
          cout << "Zero and branch is: " << dp.zero_and_Branch.read() << endl;
          cout << "Mux jump in: " << dp.mux_jump_in.read() << endl;
          cout << "Jump address: " << dp.jump_addr.read() << endl;
          cout << "PC in: " << dp.PC_in.read() << endl << endl;
      }
    }
  }
  
 private:

  void rst_thread()
  {
    rst_n.write(0);
    wait(2*clk_semiperiod);
    rst_n.write(1);
  }

  void clk_thread()
  {
    unsigned num_instr = 0;
    clk.write(0);
    while(num_instr < tot_istr)
    {
      wait(clk_semiperiod);
      if(clk.read() == 0) 
      {
        clk.write(1);
        if(rst_n.read())
          num_instr++;
      }
      else clk.write(0);
    }
  }
};

int sc_main(int argc, char** argv) 
{
  TestBench test("test");

  sc_start();

  return test.check();
}

