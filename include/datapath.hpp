#ifndef datapath_HPP
#define datapath_HPP

#include "PC.hpp"
#include "Adder.hpp"
#include "instr_mem.hpp"
#include "creater_jump_addr.hpp"
#include "mux2x5.hpp"
#include "reg_file.hpp"
#include "sign_ext.hpp"
#include "mux2x32.hpp"
#include "ALU.hpp"
#include "ALU_CTRL.hpp"
#include "data_memory.hpp"
#include "CTRL_UNIT.hpp"

#include <systemc.h>
#include <iostream>

using namespace std;
using namespace sc_core;

SC_MODULE(datapath)
{
  sc_in<bool> clk;
  sc_in<bool> rst_n;
  sc_in<sc_bv<32> > one;

  sc_signal<sc_bv<32> >  PC_in, PC_out, Instruction, PC1, mux_jump_in;
  sc_signal<sc_bv<5> >   Write_Reg_RF;
  sc_signal<sc_bv<32> >  RF_out1, RF_out2, ALU_in, ALU_out, DM_out, Write_data;
  sc_signal<bool>        RegDst, ALUSrc, MemtoReg, Jump, RegWrite, MemRead, MemWrite, Branch, 
                          Zero_ALU, and_out;
  sc_signal<sc_bv<2> >   ALU_Op;
  sc_signal<sc_bv<3> >   ALU_ctrl_out;
  sc_signal<sc_bv<32> >  Sign_ext_out, adder_in, adder_out, jump_addr;

  sc_signal<sc_bv<6> >   control_word, ALU_CTRL_in;
  sc_signal<sc_bv<5> >   RF_read_reg1, mux5A, mux5B;
  sc_signal<sc_bv<16> >  offset;
  sc_signal<sc_bv<26> >  cja_istr;
  sc_signal<sc_bv<4> >   cja_pc4;
  sc_signal<bool>        zero, zero_and_Branch;
  sc_signal<sc_bv<6> >   funct;

  reg PC;
  Adder add_PC, add_branch;
  instr_mem IM;
  creater_jump_addr cja_unit;
  mux2x5 mux_RF;
  mux2x32 MUX_ALU, MUX_DM, MUX_branch, MUX_jump;
  reg_file RF;
  sign_ext se;
  ALU ALU_unit;
  CTRL_UNIT CTRL;
  ALU_CTRL ALU_control;
  DM data_mem;

  SC_CTOR(datapath) : PC("PC"), add_PC("add_PC"), add_branch("add_branch"), IM("IM"), 
                      cja_unit("cja_unit"), mux_RF("mux_RF"), MUX_ALU("MUX_ALU"), MUX_DM("MUX_DM"), 
                      MUX_branch("MUX_branch"), MUX_jump("MUX_jump"), RF("RF"), se("se"), CTRL("CTRL"),
                      ALU_unit("ALU_unit"), data_mem("data_mem"), ALU_control("ALU_control")
  {
    PC.clk(this->clk);   
    PC.rst_n(this->rst_n); 
    PC.in(this->PC_in);
    PC.out(this->PC_out);

    add_PC.A(this->PC_out);
    add_PC.B(this->one);
    add_PC.Y(this->PC1);

    IM.indirizzo(this->PC_out); 
    IM.istruzione(this->Instruction);

    
    mux_RF.A(this->mux5A);
    mux_RF.B(this->mux5B);
    mux_RF.sel(this->RegDst);
    mux_RF.Y(this->Write_Reg_RF);

    RF.clk(this->clk); 
    RF.read_reg1(this->RF_read_reg1);
    RF.read_reg2(this->mux5A);
    RF.write_reg(this->Write_Reg_RF);
    RF.write_data(this->Write_data);
    RF.reg_write(this->RegWrite);
    RF.read_data1(this->RF_out1);
    RF.read_data2(this->RF_out2);

    se.in(this->offset);    
    se.out(this->Sign_ext_out);

    MUX_ALU.A(this->RF_out2);
    MUX_ALU.B(this->Sign_ext_out);
    MUX_ALU.sel(this->ALUSrc);
    MUX_ALU.Y(this->ALU_in);

    ALU_unit.op1(this->RF_out1);
    ALU_unit.op2(this->ALU_in);
    ALU_unit.control(this->ALU_ctrl_out);
    ALU_unit.res(this->ALU_out);
    ALU_unit.zero(this->zero);

    data_mem.address(this->ALU_out);
    data_mem.mem_write(this->MemWrite);
    data_mem.mem_read(this->MemRead);
    data_mem.write_data(this->RF_out2);
    data_mem.read_data(this->DM_out);

    MUX_DM.A(this->ALU_out);
    MUX_DM.B(this->DM_out);
    MUX_DM.sel(this->MemtoReg);
    MUX_DM.Y(this->Write_data);

    
    cja_unit.instr(this->cja_istr);
    cja_unit.pc4(this->cja_pc4);
    cja_unit.jump_addr(this->jump_addr);

    add_branch.A(this->PC1);
    add_branch.B(this->Sign_ext_out);
    add_branch.Y(this->adder_out);

    MUX_branch.A(this->PC1);
    MUX_branch.B(this->adder_out);
    MUX_branch.sel(this->zero_and_Branch);
    MUX_branch.Y(this->mux_jump_in);

    MUX_jump.A(this->mux_jump_in);
    MUX_jump.B(this->jump_addr);
    MUX_jump.sel(this->Jump);
    MUX_jump.Y(this->PC_in);

    ALU_control.ALU_Op(this->ALU_Op);
    ALU_control.funct(this->funct);
    ALU_control.Operation(this->ALU_ctrl_out);

    CTRL.Operation(this->control_word);
    CTRL.RegDst(this->RegDst);
    CTRL.ALUSrc(this->ALUSrc);
    CTRL.MemtoReg(this->MemtoReg);
    CTRL.Jump(this->Jump);
    CTRL.RegWrite(this->RegWrite);
    CTRL.MemRead(this->MemRead);
    CTRL.MemWrite(this->MemWrite);
    CTRL.Branch(this->Branch);
    CTRL.ALU_Op(this->ALU_Op);

    SC_THREAD(set_parts_of_signals);
      sensitive << Instruction << PC1;

    SC_THREAD(zero_and_Branch_thread);
      sensitive << zero << Branch;
  }

  sc_bv<32> RF_Loc(unsigned i) const {return RF.Loc(i);}
  sc_bv<32> DM_Loc(unsigned i) const {return data_mem.Loc(i);}

 private:
  void set_parts_of_signals();
  void zero_and_Branch_thread();

};

#endif
