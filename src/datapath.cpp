#include <systemc.h>
#include "datapath.hpp"

using namespace std;
using namespace sc_core;

void datapath::set_parts_of_signals()
{
   while(true)
   {
      wait();
      mux5A.write(Instruction.read().range(20,16));
      mux5B.write(Instruction.read().range(15,11));
      RF_read_reg1.write(Instruction.read().range(25,21));
      offset.write(Instruction.read().range(15,0));
      cja_istr.write(Instruction.read().range(25,0));
      cja_pc4.write(PC1.read().range(31,28));
      funct.write(Instruction.read().range(5,0));
      control_word.write(Instruction.read().range(31,26));
   }
}

void datapath::zero_and_Branch_thread()
{
   while(true)
   {
      wait();
      zero_and_Branch.write(zero.read() & Branch.read());
   }
}